//
//  AnalyticsTrackable.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 29.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Firebase

protocol AnalyticEvent: AnalyticsTrackable {
    var method: String { get }
    var parameters: [String: Any] { get }
}

protocol AnalyticsTrackable {
    func track()
}

extension AnalyticEvent {
    func track() {
        Analytics.logEvent(method,
                           parameters: parameters)
    }
}
