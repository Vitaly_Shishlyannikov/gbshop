//
//  TabBarClick.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 29.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

struct TabClick: AnalyticEvent {
    var method: String { "TabClick" }
    
    var parameters: [String : Any] {
        ["selected_tab": selectedTab,
         "date": Date().timeIntervalSince1970]
    }
    
    var selectedTab: Int = 0
}
