//
//  MainScreenShowed.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 29.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

struct MainScreenShowed: AnalyticEvent {
    var method: String {"MainScreenShowed"}
    
    var parameters: [String : Any] {
        ["date": Date().timeIntervalSince1970]
    }
}
