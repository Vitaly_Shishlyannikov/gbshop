//
//  UITabBar.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 25.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 83
        return sizeThatFits
    }
}
