//
//  UIViewController + HideKeyboard.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 01.06.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

extension UIViewController {
    open func setKeyboardHiding() {
        let tap = UITapGestureRecognizer(target: self.view,
                                         action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
}
