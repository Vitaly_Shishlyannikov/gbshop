//
//  SystemColor.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 19.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

extension UIColor {
    
    public class var customBackgroundColor: UIColor {
        return UIColor(red: 0.976, green: 0.976, blue: 0.976, alpha: 1)
    }
}
