//
//  SystemColor.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 19.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

extension UIColor {
    
    public class var customBackgroundColor: UIColor {
        return UIColor(red: 0.976, green: 0.976, blue: 0.976, alpha: 1)
    }
    
    public class var grayText: UIColor {
        return UIColor(red: 0.608, green: 0.608, blue: 0.608, alpha: 1)
    }
    
    public class var blackText: UIColor {
        return UIColor(red: 0.175, green: 0.175, blue: 0.175, alpha: 1)
    }
    
    public class var customRed: UIColor {
        return UIColor(red: 0.859, green: 0.188, blue: 0.133, alpha: 1)
    }
}
