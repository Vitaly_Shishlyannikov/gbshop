//
//  RegistrationViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 14.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol RegistrationViewControllerProtocol: class {
}

class RegistrationViewController: UIViewController {
    
    @IBOutlet private weak var userIDTextfield: UITextField!
    @IBOutlet private weak var usernameTextfield: UITextField!
    @IBOutlet private weak var passwordTextfield: UITextField!
    @IBOutlet private weak var genderTextfield: UITextField!
    @IBOutlet private weak var emailTextfield: UITextField!
    @IBOutlet private weak var bioTextfield: UITextField!
    @IBAction private func regButton(_ sender: Any) {
        presenter.register(userName: usernameTextfield.text ?? "",
                           password: passwordTextfield.text ?? "",
                           email: emailTextfield.text ?? "")
    }
    @IBAction func changeButton(_ sender: Any) {
        presenter.changeInfo(userID: Int(userIDTextfield.text ?? "") ?? 0,
                             userName: usernameTextfield.text ?? "",
                             password: passwordTextfield.text ?? "",
                             email: emailTextfield.text ?? "",
                             gender: genderTextfield.text ?? "",
                             creditCardNumber: "",
                             bio: bioTextfield.text ?? "")
    }
    
    var presenter: RegistrationPresenter!
    var assembler: RegistrationAssembly!

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.assembler = RegistrationAssembly(view: self)
        assembler.assembly()
    }
}

extension RegistrationViewController: RegistrationViewControllerProtocol {
}
