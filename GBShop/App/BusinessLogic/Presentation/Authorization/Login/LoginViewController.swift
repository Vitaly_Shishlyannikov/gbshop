//
//  AuthViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol LoginViewControllerProtocol: class {
}

class LoginViewController: UIViewController {

    @IBOutlet private weak var usernameTextfield: UITextField!
    @IBOutlet private weak var passwordTextfield: UITextField!
    @IBAction private func loginBtn(_ sender: Any) {
        presenter.login(username: usernameTextfield.text ?? "", password: passwordTextfield.text ?? "")
    }
    @IBAction func logoutButton(_ sender: Any) {
        presenter.logout(userID: 123)
    }
    
    var presenter: LoginPresenterProtocol!
    var assembler: LoginAssembly!
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.assembler = LoginAssembly(view: self)
        assembler.assembly()
    }
}

extension LoginViewController: LoginViewControllerProtocol {
}



