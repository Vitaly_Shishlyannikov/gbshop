//
//  BasketPresenter.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

protocol BasketPresenterProtocol {
    func getBasket()
    func itemsList() -> [BasketItem]
    func itemsCount() -> Int
    func item(index: Int) -> BasketItem?
}

class BasketPresenter {
    
    private var items = [BasketItem]()
    private lazy var userID = 0 // KeychainSwift value
    
    private weak var view: BasketTableViewControllerProtocol?
    private let basketService: BasketService

    init(view: BasketTableViewControllerProtocol) {
        self.view = view
        self.basketService = RequestFactory().makeBasketRequestFactory()
    }
}

extension BasketPresenter: BasketPresenterProtocol {
    func getBasket() {
        self.basketService.getBasket(userID: userID) {[weak self] response in
            switch response.result {
            case .success(let list):
                self?.items = list.products
                self?.reloadView()
            case .failure(let error):
               print(error.localizedDescription)
            }
        }
    }
    
    func itemsList() -> [BasketItem] {
        return self.items
    }
    
    func itemsCount() -> Int {
        return self.items.count
    }
    
    func item(index: Int) -> BasketItem? {
        return self.items[index]
    }
    
    private func reloadView() {
        DispatchQueue.main.async {
            self.view?.reload()
        }
    }
}
