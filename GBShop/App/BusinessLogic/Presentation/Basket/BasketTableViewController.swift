//
//  BasketTableViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol BasketTableViewControllerProtocol: class {
    func reload()
}

class BasketTableViewController: UITableViewController {
    
    var presenter: BasketPresenterProtocol!
    var assembler: BasketAssembly!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.assembler = BasketAssembly(view: self)
        self.assembler.assembly()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.presenter.getBasket()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.itemsCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketCell", for: indexPath)
        guard let item = self.presenter.item(index: indexPath.row) else { return UITableViewCell() }
        
        cell.textLabel?.text = item.productName
        cell.detailTextLabel?.text = String(item.quantity) + " шт."
        
        return cell
    }
}

extension BasketTableViewController: BasketTableViewControllerProtocol {
    func reload() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
