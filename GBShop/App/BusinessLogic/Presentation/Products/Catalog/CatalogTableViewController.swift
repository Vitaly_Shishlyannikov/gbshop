//
//  CatalogTableViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 18.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol CatalogTableViewControllerProtocol: class {
    func reload()
    func showDetailsAlert(item: GoodsItem)
}

class CatalogTableViewController: UITableViewController {
    
    var presenter: CatalogPresenterProtocol!
    var assembler: CatalogAssembly!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.assembler = CatalogAssembly(view: self)
        self.assembler.assembly()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.presenter.getCatalog(offset: 0, limit: 10)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.goodsCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatalogCell", for: indexPath)
        guard let item = self.presenter.item(index: indexPath.row) else { return UITableViewCell() }
        
        let basketButton = UIButton(type: .system)
        basketButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        basketButton.addTarget(self, action: #selector(basketButtonTapped), for: .touchUpInside)
        let basketImage = UIImage(systemName: "bag.badge.plus")
        basketButton.setImage(basketImage, for: .normal)
        basketButton.tag = indexPath.row
        cell.accessoryView = basketButton

        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = String(item.price) + " руб."
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.showDetails(index: indexPath.row)
    }
    
    @objc func basketButtonTapped(sender:UIButton) {
        let index = sender.tag
        self.presenter.addToBasket(index: index)
    }
}

extension CatalogTableViewController: CatalogTableViewControllerProtocol {
    func reload() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showDetailsAlert(item: GoodsItem) {
        DispatchQueue.main.async {
            let detailAlert = UIAlertController(title: item.name, message: item.description + " \(item.price) руб", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .default, handler:nil)
            detailAlert.addAction(cancelAction)
            self.present(detailAlert, animated: true)
        }
    }
}
