//
//  CatalogPresenter.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 18.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

protocol CatalogPresenterProtocol {
    func getCatalog(offset: Int, limit: Int)
    func goodsList() -> [GoodsItem]
    func goodsCount() -> Int
    func item(index: Int) -> GoodsItem?
    func showDetails(index: Int)
    func addToBasket(index: Int)
}

class CatalogPresenter {
    
    private var goods = [GoodsItem]()
    private lazy var userID = 0 // KeychainSwift value
    private lazy var standartQuantity = 1
    
    private weak var view: CatalogTableViewControllerProtocol?
    private let productService: ProductService
    private let basketService: BasketService

    init(view: CatalogTableViewControllerProtocol) {
        self.view = view
        self.productService = RequestFactory().makeProductRequestFactory()
        self.basketService = RequestFactory().makeBasketRequestFactory()
    }
}

extension CatalogPresenter: CatalogPresenterProtocol {
    
    func getCatalog(offset: Int, limit: Int) {
        productService.getCatalog(offset: offset, limit: limit) { [weak self] response in
            switch response.result {
            case .success(let catalog):
                self?.goods = catalog.products
                self?.reloadView()
            case .failure(let error):
               print(error.localizedDescription)
            }
        }
    }
    
    func addToBasket(index: Int) {
        let item = self.goods[index]
        
        basketService.addProducts(userID: userID, productID: item.id, quantity: standartQuantity) { response in
            switch response.result {
            case .success(let message):
                print(message)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func goodsList() -> [GoodsItem] {
        return self.goods
    }
    
    func goodsCount() -> Int {
        return self.goods.count
    }
    
    func item(index: Int) -> GoodsItem? {
        return self.goods[index]
    }
    
    func showDetails(index: Int) {
        let item = self.goods[index]
        self.view?.showDetailsAlert(item: item)
    }
    
    private func reloadView() {
        DispatchQueue.main.async {
            self.view?.reload()
        }
    }
}
