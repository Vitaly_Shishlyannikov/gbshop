//
//  LoginPresenter.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol LoginPresenterProtocol: class {
    func login(email: String, password: String)
}

final class LoginPresenter {
    
    private weak var view: LoginViewControllerProtocol?
    private let authService: AuthService

    init(view: LoginViewControllerProtocol) {
        self.view = view
        self.authService = RequestFactory().makeAuthRequestFactory()
    }
}

extension LoginPresenter: LoginPresenterProtocol {
 
    func login(email: String, password: String) {
        authService.login(email: email, password: password) { response in
            switch response.result {
            case .success(let login):
                print(login)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
