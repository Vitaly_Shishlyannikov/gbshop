//
//  ForgotPasswordPresenter.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 22.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol ForgotPasswordPresenterProtocol: class {
    func restore(email: String)
}

class ForgotPasswordPresenter {
    
    private weak var view: ForgotPasswordViewControllerProtocol?
    private let authService: AuthService

    init(view: ForgotPasswordViewControllerProtocol) {
        self.view = view
        self.authService = RequestFactory().makeAuthRequestFactory()
    }
}

extension ForgotPasswordPresenter: ForgotPasswordPresenterProtocol {
 
    func restore(email: String) {
        // authService.restore
    }
}
