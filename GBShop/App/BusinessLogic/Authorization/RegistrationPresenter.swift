//
//  RegistrationPresenter.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol RegistrationPresenterProtocol: class {
    func register(userName: String,
                  password: String,
                  email: String)
}

final class RegistrationPresenter {
    
    private weak var view: RegistrationViewControllerProtocol?
    private let authService: AuthService

    init(view: RegistrationViewControllerProtocol) {
        self.view = view
        self.authService = RequestFactory().makeAuthRequestFactory()
    }
}

extension RegistrationPresenter: RegistrationPresenterProtocol {
    
    func register(userName: String,
                  password: String,
                  email: String) {
        authService.register(userName: userName,
                             password: password,
                             email: email) { response in
            switch response.result {
            case .success(let message):
                print(message)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
     

