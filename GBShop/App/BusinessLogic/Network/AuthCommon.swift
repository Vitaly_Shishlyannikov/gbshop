//
//  Auth.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 14.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

class AuthCommon: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://localhost:8080/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension AuthCommon: AuthService {
    func register(userName: String,
                  password: String,
                  email: String,
                  completionHandler: @escaping (DataResponse<RegisterResult>) -> Void) {
        let requestModel = Register(baseURL: baseUrl,
                                    userName: userName,
                                    password: password,
                                    email: email)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func login(email: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void) {
           let requestModel = Login(baseURL: baseUrl,
                                    email: email,
                                    password: password)
           self.request(request: requestModel, completionHandler: completionHandler)
    }    
    
    func logout(userID: Int, completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void) {
        let requestModel = Logout(baseURL: baseUrl, userID: userID)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func changeInfo(userID: Int,
                    userName: String,
                    password: String,
                    email: String,
                    gender: String,
                    creditCardNumber: String,
                    bio: String,
                    completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void) {
        let requestModel = ChangeInfo(baseURL: baseUrl,
                                      userID: userID,
                                      userName: userName,
                                      password: password,
                                      email: email,
                                      gender: gender,
                                      creditCardNumber: creditCardNumber,
                                      bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension AuthCommon: ProductService {
    func getCatalog(offset: Int, limit: Int, completionHandler: @escaping (DataResponse<GoodsResult>) -> Void) {
        let requestModel = GoodsCatalogRequest(baseURL: baseUrl, offset: offset, limit: limit)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension AuthCommon: BasketService {
    func getBasket(userID: Int, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void) {
        let requestModel = GetBasketRequest(baseURL: baseUrl, userID: userID)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func addProducts(userID: Int, productID: Int, quantity: Int, completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void) {
        let requestModel = AddToBasketRequest(baseURL: baseUrl,
                                              userID: userID,
                                              productID: productID,
                                              quantity: quantity)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

