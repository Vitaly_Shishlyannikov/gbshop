//
//  AuthService.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 14.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

protocol AuthService {
    
    func register(userName: String,
                  password: String,
                  email: String,
                  completionHandler: @escaping (DataResponse<RegisterResult>) -> Void)
    
    func login(email: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
    
    func logout(userID: Int, completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void)
    
    func changeInfo(userID: Int,
                    userName: String,
                    password: String,
                    email: String,
                    gender: String,
                    creditCardNumber: String,
                    bio: String,
                    completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void)
}
