//
//  Auth.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 14.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

class Auth: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension Auth: AuthService {
    func register(userID: Int, userName: String, password: String, email: String, gender: String, creditCardNumber: String, bio: String, completionHandler: @escaping (DataResponse<RegisterResult>) -> Void) {
        let requestModel = Register(baseURL: baseUrl, userID: userID, userName: userName, password: password, email: email, gender: gender, creditCardNumber: creditCardNumber, bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func login(userName: String, password: String, completionHandler: @escaping (DataResponse<LoginResult>) -> Void) {
           let requestModel = Login(baseURL: baseUrl, login: userName  , password: password)
           self.request(request: requestModel, completionHandler: completionHandler)
    }    
    
    func logout(userID: Int, completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void) {
        let requestModel = Logout(baseURL: baseUrl, userID: userID)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
    func changeInfo(userID: Int, userName: String, password: String, email: String, gender: String, creditCardNumber: String, bio: String, completionHandler: @escaping (DataResponse<SimpleAuthResult>) -> Void) {
        let requestModel = ChangeInfo(baseURL: baseUrl, userID: userID, userName: userName, password: password, email: email, gender: gender, creditCardNumber: creditCardNumber, bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

