//
//  BasketService.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

protocol BasketService {
    
    func addProducts(userID: Int,
                     productID: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<AddToBasketResult>) -> Void)
    
    func getBasket(userID: Int, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void)
}
