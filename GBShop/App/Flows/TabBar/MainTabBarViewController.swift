//
//  TabBarViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 23.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

final class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        checkScreenAnalytic()
        
        setColors()
        setCornerRadius()
        setControllers()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let selectedIndex = tabBar.items?.firstIndex(of: item) {
            TabClick(selectedTab: selectedIndex).track()
        }
    }
    
    private func setColors() {
        view.tintColor = UIColor.customRed
        view.backgroundColor = UIColor.customBackgroundColor
    }
    
    private func setCornerRadius() {
        tabBar.layer.masksToBounds = true
        tabBar.isTranslucent = false
        tabBar.layer.cornerRadius = 20
        tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    private func setControllers() {
        
        let catalogViewController = CatalogViewController()
        let catalogTabBarItem = UITabBarItem(title: "Shop",
                                             image: UIImage(named: "shop"),
                                             selectedImage: UIImage(named: "shopred"))
        catalogViewController.tabBarItem = catalogTabBarItem
        
        let bagViewController = BagViewController()
        let bagTabBarItem = UITabBarItem(title: "Bag",
                                         image: UIImage(named: "bag"),
                                         selectedImage: UIImage(named: "bagred"))
        bagViewController.tabBarItem = bagTabBarItem
        
        let profileViewController = ProfileViewController()
        let profileTabBarItem = UITabBarItem(title: "Profile",
                                             image: UIImage(named: "profile"),
                                             selectedImage: UIImage(named: "profilered"))
        profileViewController.tabBarItem = profileTabBarItem
        
        let tabBarList = [catalogViewController, bagViewController, profileViewController]
        self.viewControllers = tabBarList
        
        setTabTextAttributes(tabBarList: tabBarList)
        setTabItemsPosition(tabBarList: tabBarList)
    }

    private func setTabTextAttributes (tabBarList: [UIViewController]) {
        
        tabBarList.forEach { controller in
        controller.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "Metropolis-Medium", size: 14) as Any], for: .normal)
        }
    }
    
    private func setTabItemsPosition (tabBarList: [UIViewController]) {
        tabBarList.forEach { controller in
            controller.tabBarItem.imageInsets = UIEdgeInsets(top: -20,
                                                             left: -6,
                                                             bottom: -6,
                                                             right: -6)
            controller.tabBarItem.titlePositionAdjustment = .init(horizontal: 0,
                                                                  vertical: -15)
        }
    }
    
    private func checkScreenAnalytic() {
        MainScreenShowed().track()
    }
}
