//
//  BagViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 23.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

final class BagViewController: CommonViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
