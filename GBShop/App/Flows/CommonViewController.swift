//
//  CommonViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 19.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

class CommonViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backgroundColor = UIColor.customBackgroundColor
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.view.backgroundColor = backgroundColor
        
        self.setKeyboardHiding()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
    }
}
