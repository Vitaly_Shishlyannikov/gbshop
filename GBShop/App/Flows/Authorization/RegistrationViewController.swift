//
//  RegistrationViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 16.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol RegistrationViewControllerProtocol: class {
}

final class RegistrationViewController: CommonViewController {
    
    @IBOutlet private weak var nameInput: TwoRowsStackView!
    @IBOutlet private weak var emailInput: TwoRowsStackView!
    @IBOutlet private weak var passwordInput: OneRowStackView!
    @IBAction func signUp(_ sender: UIButton) {
    }
    
    var presenter: RegistrationPresenterProtocol!
    private var assembler: RegistrationAssembly!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitles()
        assembler = RegistrationAssembly(view: self)
        assembler.assembly()
    }
    
    private func setupTitles() {
        nameInput.addLabelText(labelText: "name")
        emailInput.addLabelText(labelText: "email")
        passwordInput.addPlaceholder(placeholder: "Password")
    }
}

extension RegistrationViewController: RegistrationViewControllerProtocol {
}
