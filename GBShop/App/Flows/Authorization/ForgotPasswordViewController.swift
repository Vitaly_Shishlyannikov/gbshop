//
//  ForgotPasswordViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 16.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol ForgotPasswordViewControllerProtocol: class {
}

final class ForgotPasswordViewController: CommonViewController {
    
    @IBOutlet private weak var emailInput: TwoRowsStackView!
    @IBAction func send(_ sender: UIButton) {
    }
    
    var presenter: ForgotPasswordPresenterProtocol!
    private var assembler: ForgotPasswordAssembly!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitles()
        assembler = ForgotPasswordAssembly(view: self)
        assembler.assembly()
        
        setAccessibility()
    }
    
    private func setupTitles() {
        emailInput.addLabelText(labelText: "email")
    }
    
    private func setAccessibility() {
        emailInput.textField.isAccessibilityElement = true
        emailInput.textField.accessibilityIdentifier = "emailRestoreInput"
    }
}

extension ForgotPasswordViewController: ForgotPasswordViewControllerProtocol {
}
