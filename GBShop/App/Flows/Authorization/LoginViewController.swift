//
//  LoginInViewController.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 16.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

protocol LoginViewControllerProtocol: class {
}

final class LoginViewController: CommonViewController {
    
    @IBOutlet private weak var emailInput: TwoRowsStackView!
    @IBOutlet private weak var passwordInput: OneRowStackView!
    @IBAction func login(_ sender: UIButton) {
        let email = emailInput.textField.text ?? ""
        let password = passwordInput.textField.text ?? ""
        presenter.login(email: email, password: password)
        
        let tabBarController = MainTabBarViewController()
        navigationController?.pushViewController(tabBarController, animated: true)
    }
    
    var presenter: LoginPresenterProtocol!
    private var assembler: LoginAssembly!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitles()
        assembler = LoginAssembly(view: self)
        assembler.assembly()
        
        setAccessibility()
    }
    
    private func setupTitles() {
        emailInput.addLabelText(labelText: "email")
        passwordInput.addPlaceholder(placeholder: "Password")
    }
    
    private func setAccessibility() {
        emailInput.textField.isAccessibilityElement = true
        emailInput.textField.accessibilityIdentifier = "emailInput"
        passwordInput.textField.isAccessibilityElement = true
        passwordInput.textField.accessibilityIdentifier = "passwordInput"
        view.accessibilityIdentifier = "loginView"
    }
}

extension LoginViewController: LoginViewControllerProtocol {
}

