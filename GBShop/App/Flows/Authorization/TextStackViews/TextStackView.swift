//
//  TextStackView.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 21.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

class TextStackView: UIStackView {

    private func setupStack() {
        
        let backgroundView = UIView(frame: bounds)
        backgroundView.backgroundColor = UIColor.white
        insertSubview(backgroundView, at: 0)
        
        backgroundView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        backgroundView.layer.shadowOpacity = 1
        backgroundView.layer.shadowRadius = 8
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.layer.masksToBounds = false
        
        self.isAccessibilityElement = false
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStack()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupStack()
    }
}
