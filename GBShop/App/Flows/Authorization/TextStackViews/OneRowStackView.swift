//
//  OneRowStackView.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 21.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

final class OneRowStackView: TextStackView {
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .none
        textField.font = UIFont(name: "Metropolis-Medium", size: 14)
        textField.textColor = UIColor.blackText
        textField.attributedPlaceholder = NSAttributedString(string: "attributed", attributes: [NSAttributedString.Key.foregroundColor: UIColor.grayText, NSAttributedString.Key.font: UIFont(name: "Metropolis-Medium", size: 14) as Any])
        return textField
    }()

    private func setupUI() {
        
        self.addArrangedSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        textField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
    }
    
    public func addPlaceholder(placeholder text: String) {
        self.textField.placeholder = text
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}

