//
//  TwoRowsTextfield.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 19.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

final class TwoRowsStackView: TextStackView {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Metropolis-Regular", size: 11)
        label.textColor = UIColor.grayText
        return label
    }()
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.autocapitalizationType = .none
        textField.font = UIFont(name: "Metropolis-Medium", size: 14)
        textField.textColor = UIColor.blackText
        return textField
    }()

    private func setupUI() {
        
        self.addArrangedSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 14).isActive = true
        
        self.addArrangedSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 29).isActive = true
        textField.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
    }
    
    public func addPlaceholder(placeholder text: String) {
        self.textField.placeholder = text
    }
    
    public func addLabelText(labelText text: String) {
        self.label.text = text
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
}
