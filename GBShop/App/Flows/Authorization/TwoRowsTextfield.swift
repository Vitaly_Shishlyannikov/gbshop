//
//  TwoRowsTextfield.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 19.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import UIKit

class TwoRowsTextfield: UITextField {

    func setup() {
        let borderWidth: CGFloat = 2
        
        layer.borderColor = UIColor.customBackgroundColor.cgColor
        layer.borderWidth = borderWidth
        
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 1)
        
        self.layer.masksToBounds = false
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
