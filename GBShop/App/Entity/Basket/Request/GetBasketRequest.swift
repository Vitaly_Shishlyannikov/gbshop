//
//  GetBasketRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct GetBasketRequest: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "basket/get_basket"
    
    let userID: Int
    
    var parameters: Parameters?{
        return [
            "user_id": userID
        ]
    }
}
