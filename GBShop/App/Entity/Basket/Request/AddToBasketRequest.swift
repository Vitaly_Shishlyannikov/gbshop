//
//  AddToBasketRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct AddToBasketRequest: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "basket/add_product"
    
    let userID: Int
    let productID: Int
    let quantity: Int
    
    var parameters: Parameters?{
        return [
            "user_id": userID,
            "product_id": productID,
            "quantity": quantity
        ]
    }
}
