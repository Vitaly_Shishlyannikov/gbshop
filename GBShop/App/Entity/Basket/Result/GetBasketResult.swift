//
//  GetBasketResult.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

struct GetBasketResult: Codable {
    let count: Int
    let amount: Int
    let products: [BasketItem]
}
