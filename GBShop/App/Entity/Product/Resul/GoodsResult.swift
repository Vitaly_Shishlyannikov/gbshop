//
//  GoodsResult.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 12.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

struct GoodsResult: Codable {
    let count: Int
    let products: [GoodsItem]
}
