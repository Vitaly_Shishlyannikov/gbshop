//
//  ProductsCatalogRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 17.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct GoodsCatalogRequest: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "product/get_products"
    
    let offset: Int
    let limit: Int
    
    var parameters: Parameters?{
        return [
            "offset": offset,
            "limit": limit
        ]
    }
}
