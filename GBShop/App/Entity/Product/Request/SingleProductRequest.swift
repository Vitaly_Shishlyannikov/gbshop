//
//  SingleProductRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 18.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct SingleProductRequest: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "goods/item"
    
    let itemCode: String
    
    var parameters: Parameters?{
        return [
            "code": itemCode
        ]
    }
}
