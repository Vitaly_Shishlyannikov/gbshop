//
//  ProductService.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 18.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

protocol ProductService {
    
    func getCatalog(offset: Int, limit: Int, completionHandler: @escaping (DataResponse<GoodsResult>) -> Void)
}
