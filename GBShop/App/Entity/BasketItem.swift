//
//  BasketItem.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

struct BasketItem: Codable {
    let userID: Int
    let productName: String
    let productPrice: Int
    let productID: Int
    let quantity: Int
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case productName = "product_name"
        case productPrice = "product_price"
        case productID = "product_id"
        case quantity
    }
}
