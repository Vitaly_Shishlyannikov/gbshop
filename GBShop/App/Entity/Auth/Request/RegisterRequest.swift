//
//  RegisterRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct Register: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "auth/register"
    
    let userName: String
    let password: String
    let email: String
    var parameters: Parameters?{
        return [
            "username": userName,
            "password": password,
            "email": email
        ]
    }
}
