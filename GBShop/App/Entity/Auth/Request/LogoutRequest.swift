//
//  LogoutRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct Logout: RequestRouter {
    var baseURL: URL
    var method: HTTPMethod = .get
    var path: String = "logout.json"
    
    let userID: Int
    var parameters: Parameters?{
        return [
            "id_user": userID
        ]
    }
}
