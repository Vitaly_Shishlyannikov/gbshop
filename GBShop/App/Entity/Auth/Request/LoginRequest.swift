//
//  LoginRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct Login: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .post
    let path: String = "auth/login"
    
    let email: String
    let password: String
    var parameters: Parameters?{
        return [
            "email": email,
            "password": password
        ]
    }
}
