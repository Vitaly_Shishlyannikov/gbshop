//
//  ChangingInfoRequest.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Alamofire

struct ChangeInfo: RequestRouter {
    let baseURL: URL
    let method: HTTPMethod = .get
    let path: String = "changeUserData.json"
    
    let userID: Int
    let userName: String
    let password: String
    let email: String
    let gender: String
    let creditCardNumber: String
    let bio: String
    var parameters: Parameters?{
        return [
            "id_user": userID,
            "username": userName,
            "password": password,
            "email": email,
            "gender": gender,
            "credit_card": creditCardNumber,
            "bio": bio
        ]
    }
}
