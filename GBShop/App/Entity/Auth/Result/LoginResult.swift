//
//  LoginResult.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 14.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

struct LoginResult: Codable {
    let accessToken: String
    let refreshToken: String
    let expires: Int
}

