//
//  GoodsItem.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 17.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

struct GoodsItem: Codable {
    let id: Int
    let name: String
    let price: Int
    let category: Int
    let quantity: Int
    let description: String
    let imageURL: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case price = "price"
        case category = "category_id"
        case quantity = "quantity"
        case description = "description"
        case imageURL = "image"
    }
}
