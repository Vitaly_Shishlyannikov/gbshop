//
//  ForgotPasswordAssembly.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 22.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

final class ForgotPasswordAssembly {
    
    private weak var view: ForgotPasswordViewController?
    
    init(view: ForgotPasswordViewController) {
        self.view = view
    }
    
    func assembly() {
        guard let view = view else { return } 
        let presenter = ForgotPasswordPresenter(view: view)
        view.presenter = presenter
    }
}
