//
//  CatalogAssembly.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 18.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

final class CatalogAssembly {
    
    private weak var view: CatalogTableViewController?
    
    init(view: CatalogTableViewController) {
        self.view = view
    }
    
    func assembly() {
        guard let view = view else { return }
        let presenter = CatalogPresenter(view: view)
        view.presenter = presenter
    }
}
