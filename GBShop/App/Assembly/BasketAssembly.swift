//
//  BasketAssembly.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 13.05.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

final class BasketAssembly {
    
    private weak var view: BasketTableViewController?
    
    init(view: BasketTableViewController) {
        self.view = view
    }
    
    func assembly() {
        guard let view = view else { return }
        let presenter = BasketPresenter(view: view)
        view.presenter = presenter
    }
}
