//
//  RegistrationAssembly.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

final class RegistrationAssembly {
    
    private weak var view: RegistrationViewController?
    
    init(view: RegistrationViewController) {
        self.view = view
    }
    
    func assembly() {
        guard let view = view else { return }
        let presenter = RegistrationPresenter(view: view)
        view.presenter = presenter
    }
}
