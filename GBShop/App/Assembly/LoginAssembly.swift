//
//  LoginAssembly.swift
//  GBShop
//
//  Created by Vitaly_Shishlyannikov on 15.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import Foundation

final class LoginAssembly {
    
    private weak var view: LoginViewController?
    
    init(view: LoginViewController) {
        self.view = view
    }
    
    func assembly() {
        guard let view = view else { return }
        let presenter = LoginPresenter(view: view)
        view.presenter = presenter
    }
}
