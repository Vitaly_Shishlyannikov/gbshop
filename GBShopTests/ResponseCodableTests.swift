//
//  ResponseCodableTests.swift
//  GBShopTests
//
//  Created by Vitaly_Shishlyannikov on 17.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import XCTest
@testable import GBShop
@testable import Alamofire

struct PostStub: Codable {
    let result: Int
    let user: User
}

enum ApiErrorStub: Error {
    case fatalError
}

struct ErrorParserStub: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return ApiErrorStub.fatalError
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}


class ResponseCodableTests: XCTestCase {
    let expectation = XCTestExpectation(description: "Download https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/login.json")
    var errorParser: ErrorParserStub!
    var authService: AuthService!
    var productService: ProductService!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        errorParser = ErrorParserStub()
        authService = RequestFactory().makeAuthRequestFactory()
        productService = RequestFactory().makeProductRequestFactory()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        errorParser = nil
        authService = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
//    }
//
//    func testShouldLoginAndParseCommon() {
//        Alamofire
//        .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/login.json")
//            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<PostStub>) in
//                switch response.result {
//                case .success(_):
//                    break
//                case .failure:
//                    XCTFail()
//                }
//                self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testShouldLoginAndParseRequest() {
//        authService.login(userName: "Somebody", password: "mypassword") { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testShouldLogoutAndParseRequest() {
//        authService.logout(userID: 123) { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testShouldRegisterAndParseRequest() {
//
//        let id = 123
//        let name = "Somebody"
//        let password = "mypassword"
//        let email = "some@some.ru"
//        let gender = "m"
//        let creditCard = "9872389-2424-234224-234"
//        let bio = "Test bio"
//
//        authService.register(userID: id,
//                             userName: name,
//                             password: password,
//                             email: email,
//                             gender: gender,
//                             creditCardNumber: creditCard,
//                             bio: bio) { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testShouldChangeUserDataAndParseRequest() {
//
//        let id = 123
//        let name = "Somebody"
//        let password = "mypassword"
//        let email = "some@some.ru"
//        let gender = "m"
//        let creditCard = "9872389-2424-234224-234"
//        let bio = "Test bio"
//
//        authService.changeInfo(userID: id,
//                               userName: name,
//                               password: password,
//                               email: email,
//                               gender: gender,
//                               creditCardNumber: creditCard,
//                               bio: bio) { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testGetProductAndParseRequest() {
//
//        productService.getCatalog(pageNumber: 1, categoryID: 1) { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
//
//    func testGetCatalogAndParseRequest() {
//
//        productService.getProductByID(productID: 1) { [weak self] response in
//            switch response.result {
//            case .success(_):
//                break
//            case .failure(_):
//                XCTFail()
//            }
//            self?.expectation.fulfill()
//        }
//        wait(for: [expectation], timeout: 10.0)
//    }
}
}
