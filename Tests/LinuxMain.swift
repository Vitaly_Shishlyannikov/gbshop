import XCTest

import GBShopTests

var tests = [XCTestCaseEntry]()
tests += GBShopTests.allTests()
XCTMain(tests)
