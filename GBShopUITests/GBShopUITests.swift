//
//  GBShopUITests.swift
//  GBShopUITests
//
//  Created by Vitaly_Shishlyannikov on 09.04.2020.
//  Copyright © 2020 Vitaly_Shishlayannikov. All rights reserved.
//

import XCTest

class GBShopUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launch()
    }
    
    // переход на tabBarController после авторизации
    func testLogin() {
        
        let loginButtonSegue = app.buttons["LoginButtonSegue"]
        loginButtonSegue.tap()
        
        let emailInput = app.textFields["emailInput"]
        emailInput.tap()
        emailInput.typeText("test")
        
        let passwordInput = app.textFields["passwordInput"]
        passwordInput.tap()
        passwordInput.typeText("test")
        
        // нажатие для скрытия клавиатуры
        let view = app.otherElements["loginView"]
        view.tap()
        
        let loginButton = app.buttons["LoginButton"]
        loginButton.tap()
        
        let tabBarsQuery = app.tabBars
        let tabButtonShop = tabBarsQuery.buttons["Shop"]
        let tabButtonBag = tabBarsQuery.buttons["Bag"]
        let tabButtonProfile = tabBarsQuery.buttons["Profile"]
        
        // проверка наличия кнопки на таббаре
        XCTAssertTrue(tabButtonShop.exists)
        
        // проверка корректной последовательности кнопок таббара
        let isCorrectPosition = (tabButtonShop.frame.maxX < tabButtonBag.frame.minX) && (tabButtonBag.frame.maxX < tabButtonProfile.frame.minX)
        XCTAssertTrue(isCorrectPosition)
    }
    
    // переход на страницу восстановления пароля через страницы регистарации и входа
    func testLongPathForgotPassword() {
        
        let signUpButton = app.buttons["SignUpButtonSegue"]
        signUpButton.tap()
        
        let haveAccountBtn = app.buttons["AlreadyHaveAccountSegue"]
        haveAccountBtn.tap()
        
        let forgotPasswordBtn = app.buttons["ForgotPasswordSegue"]
        forgotPasswordBtn.tap()
        
        let sendMailBtn = app.buttons["SendEmailButton"]
        XCTAssertTrue(sendMailBtn.exists)
    }
}
